# Ruby Programming


[**Read the book!**](https://www.gitbook.io/book/hgducharme/ruby-programming)

This book will cover the very basics of Ruby, and help introduce the Ruby syntax. It is a great introductory book for beginners, or for anyone needing answers to how to do basic things in Ruby.

<br>

### Table of Contents

* [Introduction](README.md)
* [Basic Syntax](basic_syntax/README.md)
* [Ruby Loops](ruby_loops/README.md)
* [Data Structures](arrays,_hashes,_and_blocks/README.md)
   * [Arrays](arrays,_hashes,_and_blocks/arrays.md)
   * [Hashes](arrays,_hashes,_and_blocks/hashes.md)
   * [Blocks](arrays,_hashes,_and_blocks/blocks.md)
* [Control Flow in Ruby](control_flow_in_ruby/README.md)
* [Ruby Methods](ruby_methods/README.md)
   * [Documentation](ruby_methods/documentation.md)
* [Classes](classes/README.md)
* [Procs & Lambdas](procs_&_lambdas/README.md)
* [Modules](modules/README.md)


